import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',    
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/newUserView',
    name: 'newUserView',
    component: () => import('../views/NewUserView.vue')
  },
  {
    path: '/balanceView',
    name: 'balanceView',
    component: () => import('../views/BalanceView')
  },
  {
    path: '/gastosView',
    name: 'gastosView',
    component: () => import('../views/GastosView.vue')
  },
  {
    path: '/ingresosView',
    name: 'ingresosView',
    component: () => import('../views/IngresosView.vue')
  },
  {
    path: '/userView',
    name: 'userView',
    component: () => import('../views/UserView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
